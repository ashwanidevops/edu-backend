const path = require('path');

const dotenv  = require('dotenv');

const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const feedRoutes = require('./routes/feed');
const schoolRoutes = require('./routes/school');
const standardRoutes = require('./routes/standard');
const subjectRoutes = require('./routes/subject');
const userRoutes = require('./routes/auth');
const profileRoutes = require('./routes/profile');

const app = express();

dotenv.config();

app.use(bodyParser.json());
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader(
        'Access-Control-Allow-Methods',
        'OPTIONS, GET, POST, PUT, PATCH, DELETE'
    );
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
});

app.use((req, res, next) => {
    // res.send('connected to server');
    console.log('connected to server');
    next();
})

app.use('/', feedRoutes);
app.use('/school/', schoolRoutes);
app.use('/standard/', standardRoutes);
app.use('/subject/', subjectRoutes);
app.use('/user/', userRoutes);
app.use('/', profileRoutes);

app.use((error, req, res, next) => {
    console.log(error);
    const status = error.statusCode || 500;
    const message = error.message;
    const data = error.data;
    res.status(status).json({ message: message, data: data });
});


mongoose
    .connect(
        process.env.DB_ADMIN_URL
    )
    .then(result => {
        app.listen(process.env.PORT, () => {
            console.log('Server is started');
        });
    })
    .catch(err => console.log(err));